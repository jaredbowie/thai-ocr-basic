(ns thaiocr.sentence-writing
;  (:require [thaiocr.character-reading :as character-reading :refer character-level])
  )

(comment
  "if there are maps within range"
  "-take all existing maps within range"
  "-else stop process"
  "find a new range and add 5"
  )

(defn find-all-connected-maps
"find maps within the same range"
[starting-maps]
;(println "starting-maps " starting-maps)
(let [starting-maps-no-nil (filter #(not (nil? %)) starting-maps)
      lowest-row-map (first (sort-by :lowest-row starting-maps-no-nil))
        ;starting-lowest-row (:lowest-row lowest-row-map)
        starting-highest-row (:highest-row lowest-row-map)
        ]
    (loop [keep-going true
           remaining-maps starting-maps-no-nil
           highest-row-maximum (+ 5 starting-highest-row)
           vector-of-maps []
           ]
      (if (false? keep-going)
        [(flatten vector-of-maps) remaining-maps]
        (let [all-maps-within-range (filter #(<= (:lowest-row %) highest-row-maximum) remaining-maps)
              all-maps-not-within-range (filter #(> (:lowest-row %) highest-row-maximum) remaining-maps)
              ]
          ;(println "all-maps-within-range " all-maps-within-range)
          (if (and (not (empty? all-maps-within-range)) (< highest-row-maximum 1000))
            (let [new-highest-row-maximum (:highest-row (last (sort-by :highest-row all-maps-within-range)))]
              (recur true
                     all-maps-not-within-range
                     (+ 5 new-highest-row-maximum)
                     (conj vector-of-maps all-maps-within-range)
                     ))
            (recur false
                   all-maps-not-within-range
                   0
                   vector-of-maps
                   )
            )
          )
        )
      )
    )
  )

(defn split-by-rows ;;new method better
  "split into sentences"
  [group-of-letter-maps]
  ;;basically we have the main letters creating lines
 ;;above and below that the proper level letters are associated below we can never have something that should be above.
  ;;lets find top and bottom of all level 1 characters
  (let [level1-characters (filter #(= (:level %) 1) group-of-letter-maps)
        level2-characters (filter #(= (:level %) 2) group-of-letter-maps)
        level3-characters (filter #(= (:level %) 3) group-of-letter-maps)
        level4-characters (filter #(= (:level %) 4) group-of-letter-maps)
        level5-characters (filter #(= (:level %) 5) group-of-letter-maps)
        level1-level5-combined (concat level1-characters level5-characters)
        all-level-one-five-lowest-row (map #(:lowest-row %) level1-level5-combined)
        all-level-two-lowest-row (map #(:lowest-row %) level2-characters)
        all-level-three-lowest-row (map #(:lowest-row %) level3-characters)
        all-level-four-lowest-row (map #(:lowest-row %) level4-characters)
        ;;all-level-five-lowest-row (map #(:lowest-row %) level5-characters)
        lowest (apply min all-level-one-five-lowest-row) ;;the lowest row of the top of any 1 or 5 character
        highest (apply max all-level-one-five-lowest-row) ;; the highest row of hte top of any 1 or 5 character
        line1-rangeup (+ lowest 30) ; top of the lowest + 30
        line1-rangedown (- lowest 1) ; top of the lowest - 1
        line2-rangeup (+ 1 highest) ; top of the highest + 1
        line2-rangedown (- highest 30) ; top of the highest - 30
        ]
    ;;(println highest)
    ;;(println lowest)
    (if (> 40 (- highest lowest))
      [(concat level1-characters level2-characters level3-characters level4-characters level5-characters)]
      (let [all-level-1-5-line1 (filter #(and (>= line1-rangeup (:lowest-row %)) (<= line1-rangedown (:lowest-row %))) level1-level5-combined)
            all-level-2-line1  (filter #(>= line1-rangeup (:lowest-row %)) level2-characters) ;;above vowels
            all-level-3-line1 (filter #(>= line2-rangedown (:lowest-row %)) level3-characters) ;;below vowels
            all-level-4-line1 (filter #(>= line1-rangeup (:lowest-row %)) level4-characters) ;;above accents
            ;;    all-level-5-line1 (filter #(>= line1-range (:lowest-row %)) level1-characters)
            all-level-1-5-line2 (filter #(and (>= line2-rangeup (:lowest-row %)) (<= line2-rangedown (:lowest-row %))) level1-level5-combined)
            all-level-2-line2  (filter #(< line1-rangeup (:lowest-row %)) level2-characters) ;;above vowels
            all-level-3-line2 (filter #(< line2-rangeup (:lowest-row %)) level3-characters) ;;below vowels
            all-level-4-line2 (filter #(< line1-rangeup (:lowest-row %)) level4-characters) ;;above accents
            ]
        [(concat all-level-1-5-line1 all-level-2-line1 all-level-3-line1 all-level-4-line1)  (concat all-level-1-5-line2 all-level-2-line2 all-level-3-line2 all-level-4-line2)]))

    )
  )



(defn find-patterns-in-range
  "finds the intersections of column locations to determine if each character are in the same column"
  [all-pattern-maps pattern-range]

  (let [pattern-range-as-set (set pattern-range)
        patterns-found (for [one-pattern all-pattern-maps]
                         (let [one-pattern-range (range (:lowest-column one-pattern) (:highest-column one-pattern))
                               count-one-pattern-range (count one-pattern-range)
                               one-pattern-range-as-set (set one-pattern-range)
                               intersections (clojure.set/intersection one-pattern-range-as-set pattern-range-as-set)
                               intersections-count (count intersections)
                               ]
                           (if (>= (/ intersections-count count-one-pattern-range) 0.5)
                             one-pattern
                             nil
                             ))
                         )
        patterns-found-no-nil (filter #(not (nil? %)) patterns-found)
        maybe-vec (if (not (empty? patterns-found-no-nil))
                    (vec patterns-found-no-nil)
                    nil
                    )
        ]
   ; (println maybe-vec)
   ; (println pattern-range)
    maybe-vec
                                        ;(spit "/home/jared/clojureprojects/thaiocr/vecpatterns.txt" vec-patterns-found :append true)

    )
  )

(defn write-it
  "take a vector of vectors, which contain maps"
  [vec-of-vectors]
  (apply str (for [one-vector vec-of-vectors]
               (let [the-vector-sorted (sort-by :level one-vector)]
                 (apply str (map #(:character %) the-vector-sorted))
                 )
               )
         )
  )


(defn handle-special-cases
  "search map for special cases and correct them basically remove the character following"
  [maps-sorted-by-position]
 ; (println maps-sorted-by-position)
  (loop [maps-sorted-by-position maps-sorted-by-position
         new-maps []
         ;number-to-drop 0
         ]
    (if (empty? maps-sorted-by-position)
      (do
       ; (println new-maps)
        new-maps)
      (let [first-character-map (first maps-sorted-by-position)
            first-character (:character first-character-map)
            ]
        (cond
         (= first-character "ำ" )
         (let [a-to-remove (filter #(= "า" (:character %)) maps-sorted-by-position)
               as-sorted (sort-by :lowest-column a-to-remove)
               a-removed (remove #(= (first as-sorted) %) maps-sorted-by-position)
               removed-sorted (sort-by :lowest-column a-removed)
                ]
           ; (println "two-removed " removed-sorted)
            (recur (drop 1 removed-sorted) ; drop 2 because the next one should be removed from the collection
                   (conj new-maps first-character-map)
                   ))
         (= first-character "ญ")
         (let [ying-to-remove (filter #(= "า" (:character %)) maps-sorted-by-position)
               as-sorted (sort-by :lowest-column ying-to-remove)
               ying-removed (remove #(= (first as-sorted) %) maps-sorted-by-position)
               removed-sorted (sort-by :lowest-column ying-removed)
                ]
            (recur (drop 1 removed-sorted) ; drop 2 because the next one should be removed from the collection
                   (conj new-maps first-character-map)
                   )
           )
         (or
             (= first-character "ะ") ;delete the next value in the map
             (= first-character "ฐ") ;delete the next value in the map
             )
         (do
           (recur (drop 2 maps-sorted-by-position)
                  (conj new-maps first-character-map)
                  ))
          (and (= first-character "เ") (= "เ" (:character (second maps-sorted-by-position))))
            ;untested
          (recur (drop 2 maps-sorted-by-position)
                 (conj new-maps (assoc first-character-map :character "แ"))
                 )
          :else (recur (drop 1 maps-sorted-by-position)
                 (conj new-maps first-character-map)
                 )
                                        ;(= new-character-map-found-or-edit "i") ;ignore
                                        ;(println "i")
                                        ;(= new-character-map-found-or-edit "j") ;ignore
                                        ;(println "j")
          )
        )
      )
    )

  )

(defn make-sentence-vector
  "width-of-ones-zeros = the width of the whole one-zeros-file
put each character in the proper order and print out a string
"
  [group-of-letter-maps]
  (spit "group-of-letter-maps.txt" (str (vec group-of-letter-maps)))
  (for [one-sentence-line (doall (split-by-rows group-of-letter-maps))]
    (let [use-one-sentence-line one-sentence-line  ;the vector of characters that we foun
          sorted-by-position (sort-by :lowest-column use-one-sentence-line) ; sort the vectory of characters found by position on the x axis
          all-maps-sorted-by-position-with-special-cases-handled (doall (handle-special-cases sorted-by-position))
          all-maps-sorted-by-position-no-level-2-3-4 (filter #(not (or (= 2 (:level %)) (= 3 (:level %)) (= 4 (:level %)) (= 6 (:level %)))) all-maps-sorted-by-position-with-special-cases-handled)
          ]
     ; (println all-maps-sorted-by-position-with-special-cases-handled)
      (loop [sorted-by-position all-maps-sorted-by-position-no-level-2-3-4
             to-write []
             ]
        (if (empty? sorted-by-position)
          (write-it to-write)
          (do
           ; (println (first sorted-by-position))
            (let [first-pattern (first sorted-by-position) ;the first letter map
                  ;first-pattern-x (:highest-column first-pattern)
                  ;first-pattern-x-minus-7 (- first-pattern-x 7)
                  ;first-pattern-x-plus-7 (+ first-pattern-x 3)
                  first-pattern-range (range (:lowest-column first-pattern) (+ 1 (:highest-column first-pattern)))
                  all-patterns-in-this-range (if (= 5 (:level first-pattern))
                                                 [first-pattern]
                                                 (doall (find-patterns-in-range all-maps-sorted-by-position-with-special-cases-handled first-pattern-range))
                                                 )
                  ;how-many-patterns (count all-patterns-in-this-range)
                  ]
      ;        (println all-patterns-in-this-range)
              (recur (drop 1 sorted-by-position)
                     (conj to-write all-patterns-in-this-range)
                     )
              )
            )
          )
        )
      )
    )
  )
